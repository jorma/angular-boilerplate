import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { TransferHttp } from '../../../shared/transfer-http/transfer-http';

@Component({
  selector: 'app-home-container',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  // public version: Observable<string>
  public version: string;

  constructor(private http: TransferHttp) { }

  ngOnInit() {
    // this.version = this.http.get(`backendurl`).map((data) => {
    //   return data.version;
    // });

    this.version = '0.0.1';
  }
}
