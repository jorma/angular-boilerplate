import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { TransferHttp } from '../../../shared/transfer-http/transfer-http';

@Component({
  selector: 'app-example-container',
  templateUrl: './example.component.html',
  styleUrls: ['./example.component.scss']
})
export class ExampleComponent implements OnInit {

  constructor(private http: TransferHttp) { }

  ngOnInit() { }
}
