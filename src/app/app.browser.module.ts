import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { AppModule } from './app.module';
import { BrowserTransferStateModule } from '../shared/transfer-state/browser-transfer-state.module';

@NgModule({
  bootstrap: [AppComponent],
  imports: [
    BrowserModule.withServerTransition({
      appId: 'angular-boilerplate'
    }),
    BrowserTransferStateModule,
    AppModule
  ],
  providers: []
})
export class AppBrowserModule { }
