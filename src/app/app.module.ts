import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { APP_BASE_HREF, CommonModule } from '@angular/common';

import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';

import { appRoutes } from './routes.conf';

import { HomeComponent } from './containers/Home/home.component';
import { ExampleComponent } from './containers/Example/example.component';

import { TransferHttpModule } from '../shared/transfer-http/transfer-http.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ExampleComponent
  ],
  imports: [
    CommonModule,
    HttpModule,
    TransferHttpModule,
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: !environment.production,
        initialNavigation: 'enabled'
      }
    )
  ],
  providers: [],
  exports: [AppComponent]
})
export class AppModule { }
