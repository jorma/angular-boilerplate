import { Routes } from '@angular/router';

import { HomeComponent } from './containers/Home/home.component';
import { ExampleComponent } from './containers/Example/example.component';

export const appRoutes: Routes = [
  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full',
    data: {
      title: `Angular - Boilerplate`
    }
  },
  {
    path: 'example',
    component: ExampleComponent,
    pathMatch: 'full',
    data: {
      title: `Angular - Example`
    }
  }
];
