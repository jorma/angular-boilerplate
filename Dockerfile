FROM node:6.11.2

EXPOSE 4000

RUN mkdir -p /opt/angular-boilerplate
WORKDIR /opt/angular-boilerplate

COPY package.json /opt/angular-boilerplate
RUN npm --no-color install --production

COPY . /opt/angular-boilerplate
WORKDIR /opt/angular-boilerplate/dist
CMD ["node", "server"]
